<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->model('Mainmodel');
        $this->load->model('Adminmodel');
    }
    
	public function index()
	{
        $this->logincheck();
		$this->load->view('admin/index');
	}
    public function careers_form()
	{
        $this->logincheck();
		$data = array();
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|pdf|doc|docx';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['image'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            if($this->Mainmodel->insert('tbl_careers',$_POST)){
                    $data['success'] = "Inserted Sucessfullly";
                } 
        }
        $data['career'] = $this->Mainmodel->get_all('tbl_careers');
		$this->load->view('admin/careers_form',$data);
	}
    
    public function careers()
	{
        
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            if(isset($_POST['delete'])){
               unset($_POST['delete']);
               $postedid = $_POST['id'];
               unset($_POST['id']);
                $this->Adminmodel->delete('tbl_logins',$postedid);
               redirect('admin/careers','refresh');
           }
        }
     
        $data['allcareers'] = $this->Adminmodel->get('tbl_careers',"id != ''");
		$this->load->view('admin/careers', $data);
	}
    
    public function career_view($id)
	{
        
               

        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
            $this->Adminmodel->update_all('tbl_careers',"id = '$id'",$_POST);
            $data['success'] =   "Updated Successfully";
          
        }
        $data['pagedata'] = $this->Adminmodel->get('tbl_careers',"id = '$id'");

         
		$this->load->view('admin/career_view', $data);
	}
    
    
    public function blog_form()
	{
        $this->logincheck();
		$data = array();
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|pdf|doc|docx';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['image'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            if($this->Mainmodel->insert('tbl_blog',$_POST)){
                    $data['success'] = "Inserted Sucessfullly";
                 redirect('Blog', 'refresh');
                } 
        }
        $data['blog'] = $this->Mainmodel->get_all('tbl_blog');
		$this->load->view('admin/blog_form',$data);
        
	}
    
    public function viewapplications()
	{
        $this->logincheck();
		$data = array();
   
        $data['applications'] = $this->Mainmodel->get_all('applyjob');
		$this->load->view('admin/careers_form',$data);
	}
    
    public function login(){
        $data = array();
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            extract($_POST);
            $admin = $this->Mainmodel->get_all_where('tbl_admin', "email = '$email' and pwd = '$pwd'");
            if(NULL != $admin){
                $this->session->set_userdata('admin',$admin);
                redirect('admin', 'refresh');
            }
            else{
                $data['error'] = "Invalid Username or password";
            }
        }
        $this->load->view('login',$data);
        
    }
            
            
    public function logincheck(){
        if(NULL == $this->session->userdata('admin')){
            redirect('admin/login', 'refresh');
        }

    }
     public function page()
	{
        
        $data['job'] = $this->Mainmodel->get_all('applyjob');
		$this->load->view('admin/page',$data);
	}
    
    public function settings(){
      
        $this->logincheck();
        
        $data['admin'] = $this->Mainmodel->get_all('tbl_admin');
        $this->load->view('admin/settings',$data);
        

    }
        
    public function logout()
    {
            $this->session->set_userdata('admin','');
            redirect('admin/login', 'refresh');
    }
}
