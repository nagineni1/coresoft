<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
     public function __construct()
    {
        parent::__construct();
        $this->load->model('Mainmodel');        
    }


	public function index()
	{
        $data['blog'] = $this->Mainmodel->get_all_where('tbl_blog', 'id != ""');
		$this->load->view('blog',$data);
	}
    
    public function blog_details($id = 0)
	{
         if($id == 0)
             redirect('Blog', 'refresh');
         $data['blog'] = $this->Mainmodel->get_all_where('tbl_blog', "id = '$id'");
         $data['blog_recent'] = $this->Mainmodel->get_all_where_recent('tbl_blog', "id != ''");
    
		$this->load->view('blog_details',$data);
	} 
}
