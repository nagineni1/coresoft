<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careers extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mainmodel');        
    }


	public function index()
	{

        $data['career'] = $this->Mainmodel->get_all_where('tbl_careers', 'id != ""');
		$this->load->view('careers',$data);
	}
  
    public function job($id = 0)
	{
        if($id == 0)
            redirect('Careers', 'refresh');
          if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|pdf|doc|docx';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['image'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            
                if($this->Mainmodel->insert('applyjob',$_POST)){
                    $data['success'] = "Applied Job Sucessfullly";
                } 
        }
        
        $data['career'] = $this->Mainmodel->get_all_where('tbl_careers', "id = '$id'");
        $data['career_recent'] = $this->Mainmodel->get_all_where_recent('tbl_careers', "id != ''");
        $this->load->view('job',$data);
	} 
    public function apply_form()
	{
         
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|GIF|JPG|PNG|jpeg|JPEG|pdf|doc|docx';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('image'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        
                }
                else
                {
                        $_POST['image'] = 'uploads/'.$this->upload->data()['file_name'];
                }
            
                if($this->Mainmodel->insert('applyjob',$_POST)){
                    $data['success'] = "Applied Job Sucessfullly";
                } 
        }
       
        $data['apply'] = $this->Mainmodel->get_all_where('applyjob' , 'id != ""');

		$this->load->view('apply_form',$data);
	}
  
    
    public function login(){
        $data = array();
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            extract($_POST);
            $admin = $this->Mainmodel->get_all_where('tbl_admin', "email = '$email' and pwd = '$pwd'");
            if(NULL != $admin){
                $this->session->set_userdata('admin',$admin);
                redirect('admin', 'refresh');
            }
            else{
                $data['error'] = "Invalid Username or password";
            }
        }
        $this->load->view('admin/login',$data);
        
    }
            
            
    public function logincheck(){
        if(NULL == $this->session->userdata('admin')){
            redirect('Careers/login', 'refresh');
        }

    }
}
