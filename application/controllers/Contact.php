<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('contact');
	}
    public function sendmail()
    {
       
   if ($_SERVER['REQUEST_METHOD'] === 'POST') {

         $to = "info@coresofttech.com";
         $subject = "Request received from www.coresofttech.com//";
         $message = "<h5>Core Software Technologies </h5>";

         foreach($_POST as $keys => $values){
              $message .= $keys.': '.$values.'<br>';
         }
         $header = "From:info@coresofttech.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
       
         $retval = mail ($to,$subject,$message,$header);
       
         if( $retval == true ) {
            echo "<h2 class='mss'>Message sent successfully...</h2>";
          
         }else {
            echo "<h2 class='mcs'>Message could not be sent...</h2>";
         }
  
    }
    redirect("Welcome","refresh:5");
  
        }
}
?>
