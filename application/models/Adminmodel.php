<?php

Class Adminmodel extends CI_Model {



        public function get($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_price_sum($table,$condition)
        {
            $this->db->select('sum(price) as sum');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_leads($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->join("tbl_transactions","tbl_transactions.leads_id = tbl_leads.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_leads_count($table,$condition)
        {
            $this->db->select('count(tbl_leads.id) as count');
            $this->db->from($table);
            $this->db->join("tbl_transactions","tbl_transactions.leads_id = tbl_leads.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_join($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->join("tbl_fields","tbl_fields.tbl_group_fields_id = tbl_group_fields.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_join_serve($table,$condition)
        {
            $this->db->select('*,tbl_group_fields_serve.id as groupid,tbl_fields.id as fieldid');
            $this->db->from($table);
            $this->db->join("tbl_fields","tbl_fields.tbl_group_fields_serve_id = tbl_group_fields_serve.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_field($table,$condition)
        {
            $this->db->select('*,tbl_logins.mobile as salesmobile,tbl_leads.mobile as clientmobile,tbl_leads.email as clientemail');
            $this->db->from($table);
            $this->db->join("tbl_leads","tbl_transactions.leads_id = tbl_leads.id");
            $this->db->join("tbl_logins","tbl_transactions.trxn_assignedto = tbl_logins.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_assign($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->join("tbl_leads","tbl_transactions.leads_id = tbl_leads.id");
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_order($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $this->db->order_by("sort_by", "asc");
            $query = $this->db->get();
            return $query->result();
        }
        public function get_order_asc($table,$condition,$element)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $this->db->order_by($element, "ASC");
            $query = $this->db->get();
            return $query->result();
        }
        public function get_order_desc($table,$condition,$element)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $this->db->order_by($element, "DESC");
            $query = $this->db->get();
            return $query->result();
        }
    
        public function get_image($select,$table,$condition)
        {
            $this->db->select($select);
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
                return $query->result();
        }
    public function get_cars($table,$condition)
        {
            $this->db->select('carslist.*,firstname,lastname,email,mobile,address');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
                return $query->result();
        }
     public function get_count($table,$condition)
        {
            $this->db->select('count(*) as count');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
    
    
        public function get_soldcars($table,$condition)
        {
            $this->db->select('carslist.DisplayImage,carslist.Title, transactions.*');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
                return $query->result();
        }
    
    
    public function get_sumprice($table,$condition)
        {
            $this->db->select('SUM(Price) as totalprice');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
       public function get_sumprofit($table,$condition)
        {
            $this->db->select('SUM(Profit) as totalprofit');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
    


// Read data using username and password
public function login($data) {

$condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
$this->db->select('*');
$this->db->from('users');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}
    
// Read data using username and password
public function login_admin($data) {

$condition = "email =" . "'" . $data['email'] . "' AND " . "password =" . "'" . $data['password'] . "'";
$this->db->select('*');
$this->db->from('admin');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}

// Read data from database to show data in admin page
public function read_user_information($email) {
$condition = "email =" . "'" . $email . "'";
$this->db->select('*');
$this->db->from('users');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}

    
// Read data from database to show data in admin page
public function count_members() {
$this->db->select('count(*) AS count');
$this->db->from('users');
$query = $this->db->get();
return $query->result();
} 
    
// Read data from database to show data in admin page
public function read_members() {
$this->db->select('*');
$this->db->from('users');
$query = $this->db->get();
return $query->result();
} 
    

// Read data from database to show data in admin page
public function read_admin_information($email) {

$condition = "email =" . "'" . $email . "'";
$this->db->select('*');
$this->db->from('admin');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}


// Read operators from database to show data in recharge page
public function read_single_user($data) {
$this->db->select('*');
$this->db->from('users');
$this->db->where($data);

$query = $this->db->get();

if ($query->num_rows() != 0) {
return $query->result();
} else {
return false;
}
}
    






// Read datacard operators from database to show data in recharge page
public function read_all_users() {

$this->db->select('*');
$this->db->from('users');

$query = $this->db->get();

if ($query->num_rows() != 0) {
return $query->result();
} else {
return false;
}
}
    
    public function insert($table,$data)
        {
            $this->db->insert($table, $data); 
            $insert_id = $this->db->insert_id();
            return  $insert_id;
        }
    
    

  public function update_all($table,$select,$data)
        {
            $this->db->set($data);        
            $this->db->where($select);
            $this->db->update($table);
            return true;
        }


      public function delete($table,$id)
        {
           $this->db->where('id', $id);
           $this->db->delete($table); 
        }
    public function delete_select($table,$select,$id)
        {
           $this->db->where($select, $id);
           $this->db->delete($table); 
        }
    public function delete_page($table,$id)
        {
           $this->db->where('page_id', $id);
           $this->db->delete($table); 
        }
    
    
      public function deletepre($table,$id)
        {
           $this->db->where('premium_id', $id);
           $this->db->delete($table); 
        }
     public function deleteoff($table,$id)
        {
           $this->db->where('latest_id', $id);
           $this->db->delete($table); 
        }
    
    




}




?>