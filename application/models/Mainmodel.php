<?php 
Class Mainmodel extends CI_Model {



        public function insert($table,$data)
        {
            $this->db->insert($table,$data);
            return true;
        }
        
        public function get_all($table)
        {
            $this->db->select('*');
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
        }
        public function get_all_where($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $query = $this->db->get();
            return $query->result();
        }
    
        public function get_all_where_recent($table,$condition)
        {
            $this->db->select('*');
            $this->db->from($table);
            $this->db->where($condition);
            $this->db->order_by('reg_date', 'DESC');
            $this->db->limit(3);
            $query = $this->db->get();
            return $query->result();
        }
       
        public function update_all($table,$select,$data)
        {
            $this->db->set($data);       
            $this->db->where($select);
            $this->db->update($table);
            return true;
        }
   
   
        public function delete($table,$id)
        {
           $this->db->where('id', $id);
           $this->db->delete($table);
            return true;
        }
    }