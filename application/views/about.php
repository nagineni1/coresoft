<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/about.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="features" class="features">
                <div class="container">
                    <div class="row">
                        <div class="main_features fix roomy-70">
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon2.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>IOT</h3>
                                        <p>Enable remote monitoring for millions of patients, analyze trends to create delightful shopping experiences, or save jillions by pro-actively managing your critical machinery </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon4.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>AWS</h3>
                                        <p>AWS Managed Services provides ongoing management of your AWS infrastructure so you can focus on your applications.AWS Managed Services improves agility & reduces cost.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon3.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>Chatbot</h3>
                                        <p>A chatbot platform is a pre-designed framework that tries to simplify bot-making components for users. The year 2016 was chatbot’s year.We suggest keeping the design simple</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->
            <!--Business Section-->
            <section id="business" class="business bg-grey roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="main_business">
                            <!--<div class="col-md-6">
                                <div class="business_slid">
                                    <div class="slid_shap bg-grey"></div>
                                    <div class="business_items text-center">
                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="<?php echo base_url()?>assets/images/about-img1.jpg" alt="" />
                                            </div>
                                        </div>

                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="<?php echo base_url()?>assets/images/about-img1.jpg" alt="" />
                                            </div>
                                        </div>

                                        <div class="business_item">
                                            <div class="business_img">
                                                <img src="<?php echo base_url()?>assets/images/about-img1.jpg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="col-md-6">
                                <div class="business_slid">
                                    <img src="<?php echo base_url()?>assets/images/about-img1.jpg" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase">About us:</h2>
                                    <p class="m-top-20">Core Software Technologies has practical experience in giving best in class business innovation answers for associations. Since several years, we are empowering organizations with integrated technologies and to enable them to meet the advancing business requests. </p>
                                    <p>We cooperate with associations and create solid and long term relationships. Our evolution in thought-administration, very qualified and experienced business innovation specialists.</p>

                                    <div class="business_btn">
                                        <a href="<?php echo base_url()?>Contact" class="btn btn-primary m-top-10">Contact Us</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="brand" class="brand fix roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="main_brand text-center">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img1.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img2.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img3.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img4.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img5.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img6.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>envato.com</h6>
                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <button type="button" class="btn btn-info btn-lg btn btn-default" data-toggle="modal" data-target="#myModal">Get in touch</button>
                          <!-- Modal -->
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                
                                <div class="modal-body">
                                  <form id="contact-form" method="post" action="<?php echo base_url()?>Contact/sendmail" role="form">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Firstname *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Lastname *</label>
                                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_phone">Phone</label>
                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Message *</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="action_btn text-left sm-text-center">
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
                                </div>
                              </div>

                            </div>
                          </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>