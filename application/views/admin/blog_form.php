<?php include "includes/header.php"; ?>
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="row">
                    <h3 class="title1">Add new Blog</h3>
                        
                        <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                        <form id="contact-form" method="POST" enctype="multipart/form-data">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input id="title" type="text" name="title" class="form-control" placeholder="Please enter title" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Author Name</label>
                                        <input id="name" type="text" name="name" class="form-control" placeholder="Please enter author name" required="required" data-error="Lastname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea id="summernote" type="text" name="description" class="form-control" placeholder="Please enter description" required="required" ></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                        <label for="image">Image</label>
                                        <input id="image" type="file" name="image" class="form-control" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-6">
                                    <div class="action_btn text-left sm-text-center">
                                     <input type="submit" class="btn btn-default" value="Submit">
                                </div>
                                </div>
                        </div>

                    </form>
                    </div>
			</div>
		</div>
		<?php include "includes/footer.php"; ?>