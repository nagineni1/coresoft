<?php include "includes/header.php"; ?>
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
                <h3 class="title1">All Careers</h3>
				<div class="row">
                       <?php if(isset($errors)){ ?>   
                                    <div class="alert alert-danger"><?php echo $errors; ?></div>
                  <?php } ?>
                  <?php if(isset($success)){ ?>   
                                    <div class="alert alert-success"><?php echo $success; ?></div>
                  <?php }  ?>
                            <div class="table-responsive">
                                <table class="table table-striped" id="datatable">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Title</th>
                                            <th>Location</th>
                                            <th>Responsibilities</th>
                                            <th>Experience</th>
                                            <th>View</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sr=1; foreach($allcareers as $pagevalues){ ?> 
                                        <tr>
                                           
                                                <td><?php echo $sr; ?></td>
                                                <td>
                                                     <?php echo $pagevalues->title; ?>
                                                </td>
                                                <td>
                                                     <?php echo $pagevalues->location; ?>
                                                </td>
                                                <td>
                                                     <?php echo $pagevalues->responsibilities; ?>
                                                </td>
                                                <td>
                                                     <?php echo $pagevalues->experience; ?>
                                                </td>
                                                
                                            <td><a href="<?php echo base_url("$thisclass/career_view/").$pagevalues->id; ?>"><button  class="btn btn-info">View</button></a></td>
                                                <td>
                                                     <form method="post">
                                                    <input type="hidden" name="id" value="<?php echo $pagevalues->id; ?>">
                                                    <button name="delete" type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                    </form> 
                                                         </td>
                                        </tr>
                                    <?php $sr++; } ?>
                                     
                                       
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    
                    
                    
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
        <script>
        $(document).ready(function(){
            $('#datatable').DataTable();
        });
        </script>
		<?php include "includes/footer.php"; ?>