<?php include "includes/header.php"; ?>
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
                <h3 class="title1">Add A Job Posting</h3>
				<div class="row">
                        
                        <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                        <form id="contact-form" method="POST" enctype="multipart/form-data">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input id="title" type="text" name="title" class="form-control" placeholder="Please enter title" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input id="location" type="text" name="location" class="form-control" placeholder="Please enter location" required="required" data-error="Lastname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="responsibilities">Responsibilities</label>
                                        <textarea id="form_message" name="responsibilities" class="form-control" placeholder="Responsibilities..." rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="experience">Experience</label>
                                        <textarea id="experience" name="experience" class="form-control" placeholder="Experience..." rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="conditions">Conditions</label>
                                        <textarea id="conditions" name="conditions" class="form-control" placeholder="Conditions..." rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input id="image" type="file" name="image" class="form-control" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="action_btn text-left sm-text-center">
                                     <input type="submit" class="btn btn-default" value="Submit">
                                </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    </div>
			</div>
		</div>
		<?php include "includes/footer.php"; ?>