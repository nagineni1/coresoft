<!--footer-->

		<div class="footer">
		   <p>&copy; 2016 Coresoft. All Rights Reserved | Design by <a href="#" target="_blank">Mbrinformatics</a></p>
		</div>
        <!--//footer-->
	</div>
	<!-- Classie -->
		<script src="<?php echo base_url(); ?>adminassets/js/classie.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
		<script>
$(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'Enter Here',
        tabsize: 2,
        height: 300
      });
});
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>adminassets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>adminassets/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>adminassets/js/bootstrap.js"> </script>
</body>
</html>