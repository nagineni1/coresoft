<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>adminassets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>adminassets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url(); ?>adminassets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="<?php echo base_url(); ?>adminassets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>adminassets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts--> 
<!--animate-->
<link href="<?php echo base_url(); ?>adminassets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>adminassets/js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- chart -->
<script src="<?php echo base_url(); ?>adminassets/js/Chart.js"></script>
<!-- //chart -->
<!--Calender-->
<link rel="stylesheet" href="<?php echo base_url(); ?>adminassets/css/clndr.css" type="text/css" />
<script src="<?php echo base_url(); ?>adminassets/js/underscore-min.js" type="text/javascript"></script>
<script src= "<?php echo base_url(); ?>adminassets/js/moment-2.2.1.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>adminassets/js/clndr.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>adminassets/js/site.js" type="text/javascript"></script>
        <link href="<?php echo base_url(); ?>assets/datatables/jquery.dataTables.min.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/datatables/jquery.dataTables.min.js"></script>
<!--End Calender-->
<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>adminassets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>adminassets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>adminassets/css/custom.css" rel="stylesheet">
    
<!--//Metis Menu -->
<!--Summernote starts here-->    
    <!-- include libraries(jQuery, bootstrap) -->


<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">

<!--Summernote ends here-->    
    
    
</head> 
    <?php $thisclass = $this->router->fetch_class(); ?>
<body class="cbp-spmenu-push">
	<div class="main-content">
		<?php include "navbar.php"; ?>
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<!--logo -->
				<div class="logo">
					<a href="<?php echo base_url(); ?>Admin">
						<h1>Coresoft</h1>
						<span>AdminPanel</span>
					</a>
				</div>
				<!--//logo-->
				<!--search-box-->
				<div class="search-box">
					<form class="input">
						<input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
						<label class="input__label" for="input-31">
							<svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
								<path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
							</svg>
						</label>
					</form>
				</div><!--//end-search-box-->
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				<div class="profile_details_left"><!--notifications of menu start -->
					
					<div class="clearfix"> </div>
				</div>
				<!--notification menu end -->
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img"><img src="<?php echo base_url(); ?>adminassets/images/a.png" alt=""> </span> 
									<div class="user-name">
										<p>Admin</p>
										<span>Administrator</span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="<?php echo base_url("admin/settings"); ?>"><i class="fa fa-cog"></i> Settings</a> </li> 
								<li> <a href="<?php echo base_url("admin/logout"); ?>"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->