<?php include "includes/header.php"; ?>
	<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
		<!-- main content start-->
                    <div class="hea_1">
                        <h2>Applications Received</h2>
                    </div>
                 
        <section class="section_padd">
                
                
<table class="table table-bordered">
  <tr>
    <th>S.No</th>
    <th>Name</th>
    <th>E-mail</th>
    <th>Phone Number</th>
    <th>Position Applied</th>
    <th>Job Type</th>
    <th>Experience</th>
    <th>Current CTC Per Annum</th>
    <th>Expected CTC Per Annum</th>
    <th>Previous Company Name</th>
    <th>Resume</th>
    <th>Technical Skills</th>
    <th>applied date</th>
  </tr>
<?php  $sr = 1;    foreach($job as $values){ ?>
  <tr>
    <td><?php echo $sr; ?></td>
    <td><?php echo $values->name; ?></td>
    <td><?php echo $values->email; ?></td>
    <td><?php echo $values->number; ?></td>
    <td><?php echo $values->position; ?></td>
    <td><?php echo $values->jobtype; ?></td>
    <td><?php echo $values->experience; ?></td>
    <td>Rs.<?php echo $values->cctc; ?></td>
    <td>Rs.<?php echo $values->expected; ?></td>
    <td><?php echo $values->previous; ?></td>
      <td><a href="<?php echo base_url().$values->image; ?>" target="_blank">Click here to view</a></td>
    <td><?php echo $values->jobdescription; ?></td>
    <td><?php echo $values->reg_date; ?></td>
  </tr>
<?php $sr++; } ?>
  
</table>
            </div>
        </section>
            </div>
</div>

<br><br><br>
		<?php include "includes/footer.php"; ?>