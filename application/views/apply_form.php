<?php include "layouts/header.php"; ?>
		<!-- main content start-->
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/career_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section><br><br><br>
        <div class="container">
		<div id="page-wrapper">
			<div class="main-page">
				<div class="row">
                        <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                        <form action="" method="post" enctype="multipart/form-data">
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="name" class='control-label'>Name</label>
                                                <input type="name" class="form-control" name="name" id="name" placeholder="Enter Your Name" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="email" class='control-label'>E-mail</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Your E-mail" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="number" class='control-label'>Phone Number</label>
                                                <input type="number" class="form-control" name="number" id="number" placeholder="Enter Your Phone Number" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="position" class='control-label'>Position Applied</label>
                                                <input type="name" class="form-control" name="position" id="position" placeholder="Enter Position Applied" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="jobtype" class='control-label'>Job Type</label>
                                                <select class="form-control" name="jobtype" id="jobtype" required>
                                                  <option>Select Your Option</option>
                                                  <option>Full Time</option>
                                                  <option>Part Time</option>
                                                  <option>Internship</option>
                                                  <option>Contract</option>
                                                </select>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="experience" class='control-label'>Experience</label>
                                                <select class="form-control" name="experience" id="experience" required>
                                                  <option>Select Your Option</option>
                                                  <option>Fresher</option>
                                                  <option>Internship</option>
                                                  <option>6 Months</option>
                                                  <option>1 Year</option>
                                                  <option>2 Years</option>
                                                  <option>3 Years</option>
                                                  <option>4 Years</option>
                                                  <option>5 Years</option>
                                                  <option>6 Years</option>
                                                  <option>> 6Years</option>
                                                </select>
                                              </div>
                                          </div>
                                      </div>
                                       <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="cctc" class='control-label'>Current CTC Per Annum</label>
                                                <input type="number" class="form-control" name="cctc" id="cctc" placeholder="Enter Your current ctc" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="expected" class='control-label'>Expected CTC Per Annum</label>
                                                <input type="number" class="form-control" name="expected" id="expected" placeholder="Enter Your expected ctc" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="previous" class='control-label'>Previous Company Name</label>
                                                <input type="text" class="form-control" name="previous" id="previous" placeholder="company Name" required>
                                              </div>
                                        </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="image">Upload Resume</label>
                                                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="fileHelp" required>
                                                <!--<small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>-->
                                              </div> 
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-12">
                                              <div class="form-group required">
                                                <label for="jobdescription" class='control-label'>Technical Skills</label>
                                                <textarea class="form-control" name="jobdescription" id="jobdescription" rows="7" required></textarea>
                                              </div>
                                          </div>
                                            <div class="transition colelem ss">
                                                <button class="submit btn btn_sub1">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
			</div>
		</div>
</div><br><br><br>
		<?php include "layouts/footer.php"; ?>