<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/amazon.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase">AWS Managed Services</h2>
                                    <p class="m-top-20">AWS Managed Services provides ongoing management of your AWS infrastructure so you can focus on your applications. By implementing best practices to maintain your infrastructure, AWS Managed Services helps to reduce your operational overhead and risk. AWS Managed Services automates common activities such as change requests, monitoring, patch management, security, and backup services, and provides full-lifecycle services to provision, run, and support your infrastructure.</p>
                                    
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/aws1.jpg" class="img-responsive img_sec_1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70 bg_c">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/aw.png" class="img-responsive img_sec_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-text-1">
                                    <h2>AWS Managed Services provides</h2>
                                    <h3>Change Management:</h3>
                                    <p>AWS Managed Services provides simple and efficient means to make controlled changes to your infrastructure.</p>
                                    <h3>Incident Management:</h3>
                                    <p>AWS Managed Services monitors the overall health of your infrastructure resources, and handles the daily activities of investigating and resolving alarms or incidents.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-text-1 f_item_text">
                                   <h3>Provisioning Management:</h3>
                                    <p>AWS Managed Services enables you to quickly and easily deploy your cloud infrastructure, and simplifies the on-demand provisioning of commonly used pre-defined cloud stacks. With an infrastructure designed to meet your application needs, AWS Managed Services’ automation and integration with your existing ITSM service catalog allows you to quickly stand up applications in either test or production environments through a self-service portal.</p>
                                    <h3>Patch Management:</h3>
                                    <p>AWS Managed Services takes care of all of your OS patching activities to help keep your resources current and secure.</p>
                                    <h3>Access Management:</h3>
                                    <p>AWS Managed Services provides rigor and control by applying AWS security best practices to your infrastructure.</p>
                                    <h3>Security Management:</h3>
                                    <p>AWS Managed Services protects your information assets and helps keep your AWS infrastructure secure.</p>
                                    <h3>Continuity Management:</h3>
                                    <p>AWS Managed Services backs up your AWS stacks at scheduled intervals that you define.</p>
                                    <h3>Reporting:</h3>
                                    <p>With AWS Managed Services, you have access to the data we are using to manage your infrastructure</p>
                                    <h3>ITSM Integration:</h3>
                                    <p>AWS Managed Services provides a baseline integration with IT Service Management (ITSM) tools such as the ServiceNow platform that makes it easier for enterprise users to get a unified view of their resources across all types of infrastructure environments.</p>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Call to  action section-->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           <?php include 'layouts/footer.php'; ?>