<?php include 'layouts/header.php'; ?>

            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/blog_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="container">
                    <div class="row">
                         <?php   foreach($blog as $values){ ?>
                        <div class="col-sm-4 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="<?php echo base_url("blog/blog_details/").$values->id; ?>" ><img src="<?php echo base_url().$values->image; ?>" alt="<?php echo $values->title; ?>"></a>
                                    <span><?php echo $values->reg_date; ?></span>
                                </div>
                                <div class="blog-text">
                                    <a href="<?php echo base_url("blog/blog_details/").$values->id; ?>" ><h4><?php echo $values->title; ?></h4></a>
                                    <p class="max-lines"><?php echo $values->description; ?></p>
                                    <a href="<?php echo base_url("blog/blog_details/").$values->id; ?>" class="btn btn-primary m-top-10">Readmore...</a>
                                    <!--<span class="blog-social"><a href="single-service.html" class="blog-readmore">Read more</a></span>-->
                                </div>
                            </div>
                        </div>
                   
                        <?php } ?>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>http://www.coresofttech.com/</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>http://www.coresofttech.com/</h6>

                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>