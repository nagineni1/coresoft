<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/blog_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="single-blog-area section-padding">
                    <div class="container">
                        <div class="row">
                            <?php   foreach($blog as $values){ ?>
                            <div class="col-sm-8">
                                <div class="single-blog-left-top wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                                    <img src="<?php echo base_url().$values->image; ?>" alt="">
                                    <h3><?php echo $values->title; ?></h3>
                                    <span><i class="fa fa-user"></i> <?php echo $values->name; ?> <span class="alarm-meta"><i class="fa fa-clock-o"></i> <?php echo $values->reg_date; ?></span></span>
                                    <div class="single-blog-top-text">
                                        <p><?php echo $values->description; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- single blog right start -->
                            <div class="col-sm-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="single-blog-right">
                                    <span class="single-blog-right-serach"><input type="text" placeholder="Search..."><span class="serch-icon"><a href="#"><i class="fa fa-search"></i></a></span></span>
                                    <div class="single-blog-news-letter">
                                        <h3>News letter</h3>
                                        Enter your email below if you want to
                                        receive our newsletter
                                        <form action="formspree.io/sample@gmail.com">
                                            <input type="text" placeholder="Email address">
                                            <button type="button">Subscribe</button>
                                        </form>
                                    </div>
                                    <div class="single-recent-post">
                                        <h3>Recent Post</h3>
                                        <?php foreach($blog_recent as $recent){ ?> 
                                        <a href="<?php echo base_url("blog/blog_details/").$recent->id; ?>" class="single-recent-items">
                                            
                                            <img src="<?php echo base_url().$recent->image; ?>" alt="">
                                            <div class="single-post-content">
                                                <h5><?php echo $recent->title; ?></h5>
                                                <span class="alarm-meta"><i class="fa fa-clock-o"></i> <?php echo $recent->reg_date; ?></span>
                                            </div>
                                        </a>
                                        <?php } ?>
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>