<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/blog_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="single-blog-area section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="single-blog-left-top wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                                    <img src="<?php echo base_url()?>assets/images/blog-img-2.jpg" alt="">
                                    <h3>How to avoid asset meltdown in nuclear power plants</h3>
                                    <span><i class="fa fa-user"></i> Jhon Hegdh <span class="alarm-meta"><i class="fa fa-clock-o"></i> 02 January, 2018</span></span>
                                    <div class="single-blog-top-text">
                                        <p>We can all imagine the complexity surrounding the workings of a nuclear power plant. Given the level of safety, reliability and regulatory requirements they face, every operation and process needs to be designed and managed with utmost care.  The same is the case with asset management.</p>
                                        <span>Asset management plays a critical role in the operations of a nuclear power plant for the following reasons:</span>
                                        <ol>
                                            <li><p>It establish processes to help improve reliability by ensuring the lowest downtime of assets possible</p></li>

                                            <li><p>It helps manage the complexity of assets to improve visibility and maintenance optimization. This ensures longer asset life and high returns on investment. This is especially critical in a nuclear power plant where equipment and infrastructure can come at a high cost.</p></li>

                                            <li><p>It helps protect the power supply, ensuring it is always available to meet demand</p></li>
                                        </ol>
                                        <span>Data from the IoT enables predictive maintenance</span>
                                        <p>Modern nuclear power plant designs include more IoT sensors than their older counterparts. With these sensors, operators have access to  raw data , which they can feed into a predictive monitoring solution and view actionable insights in real time.</p>

                                        <p>Data collected and analyzed can provide a precise picture of an asset’s state of health (Good, Fair, or Poor) – enabling the discovery of failures and potential failures that otherwise would have been impossible to spot. Predictive maintenance, based on this analysis, enables nuclear power plants to be more proactive and confident in their asset maintenance – potentially avoiding disasters or outages.</p>

                                        <p>With this added insight, plant operators have an advantage in terms of the continuous operation of plant assets and better scheduling of maintenance tasks – which translates into reduced costs.</p>
                                        <span>A specialized solution for the Energy & Utilities industry</span>
                                        <p>Asset and operational management for nuclear power is unique and requires specialized enterprise asset management software. CoresoftIoT for Energy and Utilities is an open analytics solution that includes a wide range of capabilities to meet current and future needs of nuclear power providers. CoresoftIoT for Energy and Utilities is built on a foundation of data integration. It can capture and aggregate all relevant sources of information required to run the most advanced analytics across a wide variety of use cases.</p>

                                        <p><i>– Out-of-the-box utility industry applications.</i> They apply a wide range of analytical capabilities to assess asset health and risk—historically and in real-time. It can verify connectivity models in a cost effective way. It can also provide situational awareness—from the equipment level to the grid level – and employ predictive maintenance to proactively address impending asset degradation or failure.</p>

                                    </div>
                                </div>
                                <!--<div class="single-blog-contact wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                    <h3>Leav a reaply</h3>
                                    <form action="contact.php">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Name here">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Email here">
                                            </div>
                                        </div>
                                        <textarea placeholder="post your comment here ..."></textarea>
                                        <button type="button" class="slide-btn global-hover">Post comment</button>
                                    </form>
                                </div>-->
                            </div>
                            <!-- single blog right start -->
                            <div class="col-sm-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="single-blog-right">
                                    <span class="single-blog-right-serach"><input type="text" placeholder="Search..."><span class="serch-icon"><a href="#"><i class="fa fa-search"></i></a></span></span>
                                    <div class="single-blog-news-letter">
                                        <h3>News letter</h3>
                                        Enter your email below if you want to
                                        receive our newsletter
                                        <form action="formspree.io/sample@gmail.com">
                                            <input type="text" placeholder="Email address">
                                            <button type="button">Subscribe</button>
                                        </form>
                                    </div>
                                    <div class="single-recent-post">
                                        <h3>Recent Post</h3>
                                        <a href="<?php echo base_url()?>blog/blog_details" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-1.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>Keeping it green: IoT for sustainable retail</h5>
                                                January 1, 2018
                                            </div>
                                        </a>
                                        <a href="<?php echo base_url()?>blog/blog_details1" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-2.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>How to avoid asset meltdown in nuclear power plants</h5>
                                                January 2, 2018
                                            </div>
                                        </a>
                                        <a href="<?php echo base_url()?>blog/blog_details2" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-3.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>4 inspiring stories from the front lines of asset maintenance</h5>
                                                January 3, 2018
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>