<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/blog_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="single-blog-area section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="single-blog-left-top wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                                    <img src="<?php echo base_url()?>assets/images/blog-img-3.jpg" alt="">
                                    <h3>4 inspiring stories from the front lines of asset maintenance</h3>
                                    <span><i class="fa fa-user"></i> Stanley <span class="alarm-meta"><i class="fa fa-clock-o"></i> 03 January, 2018</span></span>
                                    <div class="single-blog-top-text">
                                        <p>When I think about what makes my job exceptional, it’s that I get to work with people who infuse honesty, intelligence and integrity into everything they do. It is more than just the technology we use or the awards we win. For starters, it is the feeling of knowing that every customer we serve has the transparency they need to make better decisions and improve how their business operates. It is also the knowledge that we employ hundreds of people with disabilities including veterans. Finally, it is the ability to constantly reinvent our processes to save our customers’ money to reinvest in new projects.</p>
                                        
                                        <p>Doing the work we do – managing 8 million sq. ft of facilities space, performing thousands of vehicle repairs, operating 500,000 sq. ft of warehouse space, and managing an inventory valued at $750M – provides many opportunities for stories of innovation. Today, I wish to share some of my favorite front line stories.</p>

                                        <span>1) Meter readings go mobile</span>
                                        <p>When we assumed maintenance responsibilities at Army Base Fort Lee located in Richmond, VA, we took responsibility for meter reading and reporting for the facility.  At the outset, there were three individuals who would go and manually record the readings. They would then come back and feed the data into the system. As you can imagine, this was time-consuming and prone to error. We installed Maximo and provided hand-held devices to the technicians.</p>
                                        <p>Immediately, we saw accuracy and efficiency go through the roof! Not only were we inputting the data in real-time, we added parameters that would guide the technician with their input if it was outside expected parameters. If the reading was outside the anticipated parameter, the technician could take an image of the reading and attach it to the work order for verification, then move on to the next meter. By keeping them focused on reading meters, we gained efficiencies while allowing Maximo simultaneously create a child work order to diagnose the potential issue by specialists.</p>
                                        <p>We went from three people reading meters to one individual with disabilities – creating fantastic increases in efficiency. As a side benefit, the ability to locate via GPS, communicate via text or video allows for peace of mind as well. A win- win for all!</p>

                                        <span>2) The smartest people we work with are technicians</span>
                                        
                                        <p>In some ways, maintaining the barracks of an army base is like managing hotel rooms. Before each occupancy, the barrack must be inspected. Using “eForms”, a Skookum developed electronic form, the technician enter the barrack and perform the necessary quality inspections. Checking the lightbulbs, power outlets, confirm the a/c is working, look for holes, check the screens, etc. For each item, the technician would toggle an affirmative ‘Yes’ on the “eForm” using their iPad. With thousands of rooms to inspect, they quickly found it was very time-intensive to constantly toggle ‘Yes’.</p>

                                        <p>To improve efficiencies, the technicians proposed the idea of assuming everything works and flip the form on its head. Instead, they would only toggle ‘No’ if there was an issue. This allowed Maximo to create real-time trouble work orders, route it accordingly allowing for resources to repair.   This tiny change in process saved our customer thousands of hours increasing efficiency. All because of a simple great idea from our technician.</p>

                                        <span>3) Transparency in asset maintenance leads to client satisfaction</span>
                                        
                                        <p>For nearly any task, there is a series of steps that, if followed, will ensure the job gets done right. We knew if we could get the right “recipe” for every task, we would save hours and gain efficiencies for our customers. This is why we created automated checklists, again based on the” eForm”. These checklists give the technicians the right recipe every time to ensure high consistent quality and save time with paperwork. Over time, we were able to incorporate a history for each asset and by having this information at the technician’s fingertips, they can best service the device. All of this work is available to our customer as they too have access to our systems as well as regular reporting proactively routed to their inbox.</p>

                                        <p>Hundreds of data points could be combined to give a robust rating that fairly evaluates sustainability based on multiple factors. That way, shoppers can get the key information at a glance. No ploughing through the details. And they’re safe knowing the scores are the result of thorough research.</p>
                                    </div>
                                </div>
                                <!--<div class="single-blog-contact wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                    <h3>Leav a reaply</h3>
                                    <form action="contact.php">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Name here">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" placeholder="Email here">
                                            </div>
                                        </div>
                                        <textarea placeholder="post your comment here ..."></textarea>
                                        <button type="button" class="slide-btn global-hover">Post comment</button>
                                    </form>
                                </div>-->
                            </div>
                            <!-- single blog right start -->
                            <div class="col-sm-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="single-blog-right">
                                    <span class="single-blog-right-serach"><input type="text" placeholder="Search..."><span class="serch-icon"><a href="#"><i class="fa fa-search"></i></a></span></span>
                                    <div class="single-blog-news-letter">
                                        <h3>News letter</h3>
                                        Enter your email below if you want to
                                        receive our newsletter
                                        <form action="formspree.io/sample@gmail.com">
                                            <input type="text" placeholder="Email address">
                                            <button type="button">Subscribe</button>
                                        </form>
                                    </div>
                                    <div class="single-recent-post">
                                        <h3>Recent Post</h3>
                                        <a href="<?php echo base_url()?>blog/blog_details" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-1.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>Keeping it green: IoT for sustainable retail</h5>
                                                January 1, 2018
                                            </div>
                                        </a>
                                        <a href="<?php echo base_url()?>blog/blog_details1" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-2.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>How to avoid asset meltdown in nuclear power plants</h5>
                                                January 2, 2018
                                            </div>
                                        </a>
                                        <a href="<?php echo base_url()?>blog/blog_details2" class="single-recent-items">
                                            <img src="<?php echo base_url()?>assets/images/blog-img-3.jpg" alt="">
                                            <div class="single-post-content">
                                                <h5>4 inspiring stories from the front lines of asset maintenance</h5>
                                                January 3, 2018
                                            </div>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>