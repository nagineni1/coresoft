<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/chatbot_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase">Chatbot</h2>
                                    <p class="m-top-20">Chatbots are taking the tech world by storm. This technology which helps humans converse with computers in their native language via a computer interface </p>
                                    <p class="m-top-20">A chatbot platform is a pre-designed framework that tries to simplify bot-making components for users. The year 2016 was chatbot’s year. That doesn’t imply that people you come across may know all about bots. It is important to consider this while building a chatbot. We have shared best practices that can be leveraged for building chatbots and ultimately deliver a bot that is user-friendly.</p>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/chatbot2.jpg" class="img-responsive img_sec_1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70 bg_c">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/chatbot1.jpg" class="img-responsive img_sec_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-text-1">
                                    <p>Chatbots are taking the tech world by storm. This technology which helps humans converse with computers in their native language via a computer interface.</p>
                                    <p>One issue with chatbots’ future viability is developers’ focus on enabling more natural and human-like conversations with users. This is a lofty goal, given that AI is not yet able to deliver on the promise of natural language processing (NLP). Because most chatbots still use retrievalbased deep-learning models instead of generative ones, users are aware they are interacting with a machine.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50">
                                    <p class="m-top-20">Another issue is that, over time, chatbot novelty will wear off. When that occurs, users’ utmost concern will be how well the bot can get things done. For example, Tencent’s WeChat has pioneered the use of a messaging platform to order, purchase and pay for products and services. The main reason WeChat is very popular in Asia, especially in China, is that it brings almost every service into the messenger without the need to leave the WeChat app. It achieves all of this through embedded mobile websites rather than an army of chatbots.  </p>
                                    <p class="m-top-20">To make chatbots effective, we suggest keeping the design simple, and “conversation” to a minimum. We recommend creating utility bots that specialize in specific tasks, provide recommendations to users and excel at helping users complete those tasks. Rapid, contextual responses will be key to improving outcomes. Over time, bots will evolve from being.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--Call to  action section-->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           <?php include 'layouts/footer.php'; ?>