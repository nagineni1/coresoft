<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/cloud_banner.png" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase">Cloud Computing: </h2>
                                         <h3>Computing as a service over the internet</h3>
                                    <p class="m-top-20">Cloud computing, often referred to as simply “the cloud,” is the delivery of on-demand computing resources everything from applications to data centers over the internet on a pay-for-use basis. Most companies with private clouds will evolve to manage workloads across data centers.Metered service so you only pay for what you use.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/cloud.png" class="img-responsive img_sec_1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70 bg_c">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/cloud1.jpg" class="img-responsive img_sec_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-text-1">
                                    <h2>Software as a service (SaaS):</h2>
                                    <p>Cloud-based applications — or software as a service — run on distant computers “in the cloud” that are owned and operated by others and that connect to users’ computers via the internet and, usually, a web browser.</p>
                                   <h2>Platform as a service (PaaS)</h2>
                                    <p>Platform as a service provides a cloud-based environment with everything required to support the complete lifecycle of building and delivering...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50 f_item_text">
                                    <h2 class="text-uppercase">Infrastructure as a service (IaaS) </h2>
                                    <p class="m-top-20">Infrastructure as a service provides companies with computing resources including servers, networking, storage, and data center space on a pay-per-use basis.</p>
                                         <h3>Public cloud</h3>
                                         <p>Public clouds are owned and operated by companies that offer rapid access over a public network to affordable computing resources. With public cloud services, users don’t need to purchase hardware, software, or supporting infrastructure, which is owned and managed by providers.</p>
                                         <h3>Private cloud</h3>
                                         <p>A private cloud is infrastructure operated solely for a single organization, whether managed internally or by a third party, and hosted either internally or externally. Private clouds can take advantage of cloud’s efficiencies, while providing more control of resources and steering clear of multi-tenancy.</p>
                                         <h3>Hybrid cloud </h3>
                                         <p>A hybrid cloud uses a private cloud foundation combined with the strategic integration and use of public cloud services. The reality is a private cloud can’t exist in isolation from the rest of a company’s IT resources and the public cloud.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="features" class="features bg_c1">
                <div class="container">
                    <div class="row">
                        <div class="head_title text-center fix c f_item_text">
                            <h3 class="font-size-normal"> OUR PARTNER <small class="heading heading-solid center-block"></small></h3>
                            <p>Cloud computing cost optimizer which helps cloud consumers reduce hosting costs by 50 to 80% while keeping applications available on-demand.</p>
                            <a href="http://www.invoke.cloud/" class="btn btn-primary m-top-10">Read More</a>
                        </div>  
                        
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section>
            <!--Call to  action section-->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'layouts/footer.php'; ?>