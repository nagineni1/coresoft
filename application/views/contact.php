<style>
    .map_c{
        padding: 10px;
    }
</style>
<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/contact.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
<section class="sec-padding" style="padding: 40px;">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="smart-forms bmargin">
            <h2 class="raleway" style="color:#FC7F01;">Contact Us</h2>
            <h4>Stay in touch with us</h4>
            <br/>
            <br/>
            <form id="contact-form" method="post" action="<?php echo base_url()?>Contact/sendmail" role="form">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Firstname *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Lastname *</label>
                                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_phone">Phone</label>
                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Message *</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="action_btn text-left sm-text-center">
                                     <input type="submit" class="btn btn-default" value="Submit">
                                </div>
                                </div>
                            </div>
                        </div>

                    </form>
          </div>
          <!-- end .smart-forms section --> 
          
        </div>
        <div class="col-md-4 bmargin">
          <h3 class="raleway" style="color: #FC7F01;">Address Info</h3>
          <h6><strong>Core Software Technologies Inc.,</strong></h6>
            <h6>Head Quarters:</h6>
            666 Plainsboro Road,<br /> Suite # 1216Plainsboro,<br /> NJ 08536 <br /> <br />
            <h6>Branch:</h6>
            100 S. Murphy Ave. Suite 200,<br />Sunnyvale, CA 94086<br/> <br />
          <h6>Telephone:</h6> 1-609-256-8005<br /> <br />
          <h6>Fax:</h6> 1-609-256-8004<br /> <br />
          <h6>Mail-id:</h6> info@coresofttech.com<br />
          <div class="clearfix"></div>
          <br/>
      </div>
    </div>
  </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKrtG9DFT_ot_879p0Z9Xoohy_ETZKxW8'></script>
                        <div style='    margin-left: 0px;margin-bottom:30px;overflow:hidden;height:400px;width:580px;'>
                        <h3>Head Office</h3>
                            <div id='gmap_canvas' style='height:400px;width:580px;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='http://maps-website.com/'>maps-website</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=2495537269e2044da40a49f96c74e75da9067eab'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(40.327105,-74.57428800000002),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(40.327105,-74.57428800000002)});infowindow = new google.maps.InfoWindow({content:'<strong>Head Quarters</strong><br>666 Plainsboro Road, Suite # 1216Plainsboro, NJ 08536<br> <br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                    </div>
                    <div class="col-md-6">
                        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyA5Hr_1TzVYHdZLQaTI6uWF8P9BuMQcxI8'></script>
                        <div style='    margin-left: 30px;margin-bottom:30px;overflow:hidden;height:400px;width:580px;'>
                        <h3>Branch Office</h3>
                            <div id='gmap_canvas1' style='height:400px;width:580px;'></div><style>#gmap_canvas1 img{max-width:none!important;background:none!important}</style></div> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=8dbd500bc7d567ed3cd4cd696d47db58b8f72102'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(37.3773224,-122.02966179999999),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas1'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(37.3773224,-122.02966179999999)});infowindow = new google.maps.InfoWindow({content:'<strong>Branch Address</strong><br>100 S. Murphy Ave. Suite 200, Sunnyvale, CA 94086<br> <br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--<section>
        <div style='overflow:hidden;height:400px;width:100%;margin-bottom:-150px;'>
                <div id="map" style='height:250px;width:100%;'></div>
            </div>
                <script>
                function myMap() {
                  var myCenter = new google.maps.LatLng(40.327334, -74.574363);
                  var mapCanvas = document.getElementById("map");
                  var mapOptions = {center: myCenter, zoom: 12};
                  var map = new google.maps.Map(mapCanvas, mapOptions);
                  var marker = new google.maps.Marker({position:myCenter});
                  marker.setMap(map);

                  // Zoom to 9 when clicking on marker
                  google.maps.event.addListener(marker,'click',function() {
                    map.setZoom(15);
                    map.setCenter(marker.getPosition());
                  });
                }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMkI33H5v3omngM3IYAqJMo5VmFthaiGk&callback=myMap"></script>
    </section>-->
    <!--<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyB4pJ25XXlGRJzIpZD3tE5Tg2_eex5gaYM'></script><div style='overflow:hidden;height:400px;width:1416px;'><div id='gmap_canvas' style='height:400px;width:1416px;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='http://maps-website.com/'>maps-website</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=c29ba78cbdef1cdaebcf8a8775561fde5a94657d'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:12,center:new google.maps.LatLng(37.3773224,-122.02966179999999),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(37.3773224,-122.02966179999999)});infowindow = new google.maps.InfoWindow({content:'<strong>map</strong><br>100 S. Murphy Ave. Suite 200 Sunnyvale, CA 94086<br> Sunnyvale<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>-->
    
             <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<?php include 'layouts/footer.php'; ?>