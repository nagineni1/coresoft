<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/11.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            
            <!--Business Section-->
            <section id="business" class="business bg-grey roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="main_business">
                            <div class="col-md-8">
                                <div class="business_item sm-m-top-50">
                                    <h2 class="text-uppercase">Welcome To Employee Corner!</h2>
                                    <p class="m-top-20">This is an online community page for our employees. This section is dedicated to our employees who have questions concerning their employment with us. In this section, they can get answers of their questions, find current openings, apply to a job position or explore our work culture. </p>
                                    <p>Our ‘Employee Corner’ section specifically serves to employees needing guidance about their job. This page makes their job easier as they can get answers of their questions, find lucrative job opportunities and apply for a current job. New employees can explore our work culture in this section and find out professional ethos and work values that set us apart from the rest.</p>
                                    <p>Simply click on any of the links if you have any concerns about your job or if you are looking for a job opportunity in any department.</p>

                                    

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="myform">
                                    <!--<img src="<?php echo base_url()?>assets/images/about-img1.jpg" class="img-responsive">-->
       
                                    <?php
              if(isset($error)){
              echo  '<h4><p style="text-align: center; color: red;">Invalid credentials</p></h4>'; } 
              ?>

                                <div id="login">
                                <h2 style="text-align:center;color:#fb8109;">Login Form</h2>
                                <hr/>
                                <form action="" method="post">
                                <div class="form-group">
									<label for="name">User Name:</label>
									<input type="text" class="form-control" name="name" id="name" placeholder="Enter your name" required>
								</div>
                                    <div class="form-group">
									<label for="name">Password:</label>
									<input type="password" class="form-control" name="password" id="password" placeholder="Enter your password" required>
								</div>
                                     
                                <div class="action_btn text-left sm-text-center btnn">
                                     <input type="submit" class="btn btn-default" value="Login">
                                </div>
                                    <div class="reg">
                                    <a href="<?php echo base_url()?>Register">Register Now</a>
                                    </div>
                                </form>
                                    <div class="action_btn text-left sm-text-center btnn bb">
                                     <a href="https://rightsignature.com/forms/WeeklyProject-Rep-100633/embedded/c55da9d94f5?"><input type="submit" class="btn btn-default" value="Click Here to Employee Weekly Update"></a>
                                </div>

                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="brand" class="brand fix roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="main_brand text-center">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img1.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img2.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img3.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img4.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img5.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img6.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
               
            <?php include 'layouts/footer.php'; ?>