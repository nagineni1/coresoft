<style>
    .inc500{
    position: fixed;
    top: 120px;
    z-index: 9999;
    right: 30px;
    width: 160px;
        }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    
    
    $(window).scroll(function() {

    if ($(this).scrollTop()>0)
     {
        $('.a').fadeOut();
     }
    else
     {
      $('.a').fadeIn();
     }
 });
</script>
        <?php include 'layouts/header.php'; ?>
            <section class="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <img src="<?php echo base_url();?>assets/images/inc500.png" class="inc500 a img-responsive">
                        </div>
                    </div>
                </div>
            </section>
            <section id="home" class="home bg-black fix">
                <div class="main_home text-center">
                        <div class="hello_slid">
                            <div class="slid_item">
                                <div class="home_btns m-top-10 home_text">
                                    <h1 class="">Secure Things With Our</h1>
                                    <h1 class="">Next Generation <span>IoT</span></h1>
                                    <a href="<?php echo base_url()?>About" class="btn btn-primary m-top-20">Readmore</a>
                                </div>
                            </div><!-- End off slid item -->
                        </div>
                </div>
            </section>
            
            <section  class="home_banner">
                <img src="<?php echo base_url();?>assets/images/mobile_banner.png" class="img-responsive img_mobile">
            </section> <!--End off Home Sections-->
            <section id="features" class="features">
                <div class="container">
                    <div class="row">
                        <div class="head_title text-center fix">
                            <h2 class="font-size-normal"> WE LOVE TO SAY WHAT WE DO <small class="heading heading-solid center-block"></small></h2>
                            <h5>Core Software Technologies specializes in providing state-of-the-art business technology solutions to organizations. Since many years, we are strengthening businesses with updated technologies and to help them meet the evolving business demands.</h5>
                        </div>  
                        <div class="main_features fix roomy-70">
                            <div class="col-md-3">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon1.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>Cloud Computing</h3>
                                        <p>Cloud computing, often referred to as simply “the cloud,” is the delivery...</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon2.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>IoT</h3>
                                        <p>Enable remote monitoring for millions of patients, analyze trends... </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon3.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>Chatbot</h3>
                                        <p>Chatbots are taking the tech world by storm. This technology which helps...</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="features_item sm-m-top-30">
                                    <div class="f_item_icon">
                                        <img src="<?php echo base_url()?>assets/images/icons/icon4.png">
                                    </div>
                                    <div class="f_item_text">
                                        <h3>AWS Managed Services</h3>
                                        <p>AWS Services provides ongoing management of your AWS infrastructure... </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- End off row -->
                </div><!-- End off container -->
            </section><!-- End off Featured Section-->
            <section id="business" class="business bg-grey roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="head_title text-center fix">
                            <h2 class="font-size-normal">INTERNET of THINGS<small class="heading heading-solid center-block"></small></h2>
                            <h5>Enable remote monitoring for millions of patients, analyze trends to create delightful shopping experiences, or save jillions by pro-actively managing your critical machinery – the Internet of All Things (IoT) is here!</h5>
                        </div> 
                        <div class="col-md-12">
                            <div class="mobile_sec">
                                <div class="col-md-4">
                                    <div class="icon_text_left">
                                        <div class="ico_left">
                                            <h3 class="pr10">Automotive</h3>
                                            <img src="<?php echo base_url()?>assets/images/icons/1.png" class="img-responsive">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_left ico_text">
                                            <p>More than 60 percent of auto executives are ready to adopt cognitive IoT for innovations such as self-driving vehicles, electric cars and mobility experiences.</p>
                                        </div>
                                    </div>
                                    <div class="icon_text_left">
                                        <div class="ico_left">
                                            <h3 class="pr10">Electronics</h3>
                                            <img src="<?php echo base_url()?>assets/images/icons/2.png" class="img-responsive">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_left ico_text">
                                            <p>The electronics industry is primed to disrupt long-standing business models by infusing intelligence into devices and connecting them to virtually everything.</p>
                                        </div>
                                    </div>
                                    <div class="icon_text_left">
                                        <div class="ico_left">
                                            <h3 class="pr10">Energy and Utilities</h3>
                                            <img src="<?php echo base_url()?>assets/images/icons/3.png" class="img-responsive">
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_left ico_text">
                                            <p>Data is the key for energy and utility companies to adapt to constant change.Analyze your data better and risk to deliver safe, and sustainable energy.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <img src="<?php echo base_url()?>assets/images/phone.png" class="img-responsive w100">
                                </div>
                                <div class="col-md-4">
                                    <div class="icon_text_right">
                                        <div class="ico_right">
                                            <img src="<?php echo base_url()?>assets/images/icons/4.png" class="img-responsive">
                                            <h3 class="pl10">Insurance</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_right ico_text">
                                            <p>The volume of data generated by the IoT is staggering. Insurers need to aggregate and analyze this data proactively to shield people and their property.</p>
                                        </div>
                                    </div>
                                    <div class="icon_text_right">
                                        <div class="ico_right">
                                            <img src="<?php echo base_url()?>assets/images/icons/5.png" class="img-responsive">
                                            <h3 class="pl10">Manufacturing</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_right ico_text">
                                            <p>Increase throughput and uptime with Watson IoT™. Watson can also help you lower risk by analyzing machines, predicting outages and handling repairs.</p>
                                        </div>
                                    </div>
                                    <div class="icon_text_right">
                                        <div class="ico_right">
                                            <img src="<?php echo base_url()?>assets/images/icons/6.png" class="img-responsive">
                                            <h3 class="pl10">Retail</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ico_text_right ico_text">
                                            <p>IoT insights as a service for retail store operations and expertise in building automation contains cognitive APIs and facilities management capabilities.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-text-1">
                                    <h2>Cloud Computing</h2>
                                    <p>We give you a fail-safe Cloud Security plan that keeps chances of information-proliferation well below the threshold. Our reliable cloud security solutions abide by standards industry norms and protect all your sensitive data to the core.</p>
                                    <h4>Benefits of Cloud Integration Services:</h4>
                                    <ul class="ul_1">
                                        <li>- Easy Integration With Business Applications.</li>
                                        <li>- Seamless Flow of Information</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/cloud.png" class="img-responsive img_sec_1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70 bg_c">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/aw.png" class="img-responsive img_sec_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-text-1">
                                    <h2>AWS Managed Services</h2>
                                    <p>AWS Managed Services provides ongoing management of your AWS infrastructure so you can focus on your applications. By implementing best practices to maintain your infrastructure, AWS Managed Services helps to reduce your operational overhead and risk. AWS Managed Services automates common activities such as change requests, monitoring, patch management, security, and backup services, and provides full-lifecycle services to provision, run, and support your infrastructure. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="sec_padd-t">
                <div class="container-fluid">
                    <div class="row">
                        <div class="head_title text-center fix">
                            <h2 class="text-uppercase">We grow faster than other <small class="heading heading-solid center-block"></small></h2>
                            <h5>Core Software Technologies Inc is an emerging, U.S-based consulting company that can provide your business with clear solutions </h5>
                        </div>
                        <div class="ico_svg">
                            <svg id="svgLine" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="300" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 2000 250" preserveAspectRatio="xMinYMax">
                                <line x1="0" y1="90" x2="2500" y2="90" style="stroke:rgb(92,184,92);stroke-width:2"></line>                       
                                <text x="135" y="20" fill="#8b949b" class="svg_text">Company founded</text>
                                <text x="170" y="60" fill="#5cb85c" class="svg_year">2011</text>
                                <text x="435" y="20" fill="#8b949b" class="svg_text">Software release</text>
                                <text x="460" y="60" fill="#5cb85c" class="svg_year">2011</text>
                                <text x="735" y="20" fill="#8b949b" class="svg_text">Build bigger team</text>
                                <text x="760" y="60" fill="#5cb85c" class="svg_year">2014</text>
                                <text x="1025" y="20" fill="#8b949b" class="svg_text">750K User Registred</text>
                                <text x="1060" y="60" fill="#5cb85c" class="svg_year">2016</text>
                                <text x="1335" y="20" fill="#8b949b" class="svg_text">2.4M Revenue</text>
                                <text x="1360" y="60" fill="#5cb85c" class="svg_year">2017</text>
                                <text x="1650" y="60" fill="#333333" style="font-size: 140%; font-weight: 300; font-family: 'Open Sans', cursive;">Growing up..</text>
                                <ellipse id="svg_1" rx="15" ry="15" cx="200" cy="90" fill="#5cb85c" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_2" rx="10" ry="10" cx="500" cy="90" fill="#5cb85c" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_3" rx="15" ry="15" cx="800" cy="90" fill="#b2cc71" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_4" rx="15" ry="15" cx="1100" cy="90" fill="#3c88c6" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_5" rx="10" ry="10" cx="1400" cy="90" fill="#1abc9c" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_6" rx="10" ry="10" cx="1700" cy="90" fill="#a85ad4" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_7" rx="9" ry="9" cx="1900" cy="90" fill="#ff8b34" stroke="#ffffff" stroke-width="5"></ellipse>
                                <ellipse id="svg_8" rx="6" ry="6" cx="2500" cy="90" fill="#fd40b3" stroke="#ffffff" stroke-width="5"></ellipse>
                            </svg>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-2">
                                    <div class="action_item text-center">
                                        <h5 class="text-white text-uppercase vb">Newsletter</h5>
                                    </div>
                                </div>
                                <form method="post" action="<?php echo base_url()?>Contact/sendmail">

                                <div class="col-md-8">
                                        <input type="text" name="email" placeholder="Enter E-mail" class="form-control fmc" required>

                                </div>
                                <div class="col-md-2">
                                    <div class="action_btn text-left sm-text-center">
                                        <input type="submit" class="btn btn-default" value="Submit">
                                    </div>
                                </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-80">
                <div class="container">
                    <div class="row">
                        <div class="head_title text-center fix">
                            <h2 class="font-size-normal">GET IN TOUCH WITH US. <small class="heading heading-solid center-block"></small></h2>
                            <h5>Please feel free to say anything here.Our staff will reply as soon as possible. anything!</h5>
                            <br><br>
                        </div> 
                        <form id="contact-form" method="post" action="<?php echo base_url()?>Contact/sendmail" role="form">

                        <div class="messages"></div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Firstname *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_lastname">Lastname *</label>
                                        <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_phone">Phone</label>
                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="form_message">Message *</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="action_btn text-left sm-text-center">
                                        <input type="submit" class="btn btn-default" value="Submit">
                                </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    </div>
                </div>
            </section>
            <section id="brand" class="brand fix formss bg_c">
                <div class="container">
                    <div class="row">
                        <div class="main_brand text-center">
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/apple.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cisco.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/nec.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img style="width: 125px;height: 49px;" src="<?php echo base_url()?>assets/images/thomson.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/cbrand-img2.png" alt="" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-6">
                                <div class="brand_item sm-m-top-20">
                                    <img src="<?php echo base_url()?>assets/images/checkpoint.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'layouts/footer.php'; ?>