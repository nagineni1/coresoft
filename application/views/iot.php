<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0 iott">
                            <img src="<?php echo base_url()?>assets/images/iot-banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50">
                                    <h2 class="">Internet of Things(IoT)</h2>
                                    <p class="m-top-20">The Internet of Things (IoT) market is expected to balloon in the coming years. Research firm Gartner predicts IoT will generate $300 billion in revenue by 2020, with estimates of how many connected devices ranging from 25 billion to more than 200 billion. Those devices need sensors, networks, back-end infrastructure and analytics software to make them useful though. This list is an unscientific preview of some of the biggest companies attempting to turn the IoT market into a reality.</p>
                                    
                                </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/iot1.jpg" class="img-responsive img_sec_1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="roomy-70 bg_c">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="img_1">
                                    <img src="<?php echo base_url()?>assets/images/iot2.JPG" class="img-responsive img_sec_1">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-text-1 f_item_text">
                                    <h3>IoT</h3>
                                    <p>Enable remote monitoring for millions of patients, analyze trends to create delightful shopping experiences, or save jillions by pro-actively managing your critical machinery – the Internet of All Things (IoT) is here!</p>
                                    <p>We help you to explore a journey where your devices seamlessly interact with each other to gather, store, and process data. You may have a goal to boost your productivity, generate new revenue streams, enhance security or radically transform your service support – all of this becomes possible with IoT.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <section class="roomy-70">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-text-1">
                                     <div class="business_item sm-m-top-50 f_item_text">
                                    <h3>Building complex connected solutions is hard work. It involves:</h3>
                                    <p class="m-top-20">Bringing together disparate machines, software systems and consumers on a single platform - all speaking a common language.</p>
                                         <ul class="il">
                                            <li>Bringing together disparate machines, software systems and consumers on a single platform - all speaking a common language.</li>
                                             <li>Conducting advanced analytics on the massive amount of generated data to provide you with the intelligence and insights that will take your business to new heights.</li>
                                             <li>A scalable and flexible technology solution which can grow with your organizational needs Ensuring that minimum disruptions take place to the on-going operations of the business during implementation.</li>
                                             <li>Creating business applications that address the very specific legal, security, domain and process frameworks of your industry.</li>
                                         </ul>
                                    
                                    <h3>Customized Multi Gateway IoT Box</h3>
                                    <ul class="il">
                                         <li>Multi -Gateway is custom solution device will be used to connect several smart devices at home, office, hospitals etc. The applications such as Home Automation, Control, Monitoring and remote patient monitoring for healthcare industry can be developed utilizing the custom multi-gateway. Our concentration is to develop solutions based on Multi Gateway device which consists of several interfaces such as 3G/4G LTE, Sensors (Temperature, Door, Window, Humidity and Vital etc.), Wi-Fi (IP camera).</li>
                                        <li>Multi-Gateway device usage can be extended to different industrial (solar plants, factories etc) areas by introducing various interfaces or slimming down the current interfaces to make it cost affective. Multi-Gateway device acts as server to all connected devices around diversified areas such as Access Control Services (ACS), where this device will receive alerts when the connected devices generate the alerts for updated firmware upgrade or device malfunction. The alerts are handled by most advanced protocols such as TR069. The project plan includes the converting of Multi Gateway device into TR069 protocol processor and protocol analyzer.</li>
                                        <li>The following solutions are underway in development</li>
                                        </ul>
                                        <ol class="ilo">
                                          <li> Single/Multi Home Automation/Control/Monitoring Solution Applications</li>  
                                          <li> Patient monitoring system application</li>
                                        <li>Access Control Service using advanced TR069 protocol.</li>
                                        </ol>
                
                                </div>
                                        <img src="<?php echo base_url()?>assets/images/iotdemo.png" class="img-responsive">
                                    <p>For questions/feedback, please either call us or send an email to IoT-Info@CoreSoftTech.com</p>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </section>
            <!--Call to  action section-->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">Let's get started on your project</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include 'layouts/footer.php'; ?>