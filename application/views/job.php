<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/career_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="single-blog-area section-padding">
                    <div class="container">
                        <div class="row">
                            <?php   foreach($career as $values){ ?>
                            <div class="col-sm-8">
                                <div class="single-blog-left-top wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                                    <img src="<?php echo base_url().$values->image; ?>" alt="">
                                    <h3>Web Designer</h3>
                                    <span><i class="fa fa-map"></i><?php echo $values->location; ?> <span class="alarm-meta"><i class="fa fa-clock-o"></i> <?php echo $values->reg_date; ?></span></span>
                                    <div class="col-md-12 job_sec">
                                        <div class="col-md-5">
                                            <h4>Responsibilities</h4>
                                        </div>
                                        <div class="col-md-7 job_sec_p">
                                            <p><?php echo $values->responsibilities; ?></p>
                                        </div>
                                        <hr class="hrs">
                                        <div class="col-md-5">
                                            <h4>Experience</h4>
                                        </div>
                                        <div class="col-md-7 job_sec_p">
                                            <p><?php echo $values->experience; ?></p>
                                        </div>
                                        <hr class="hrs">
                                        <div class="col-md-5">
                                            <h4>Conditions</h4>
                                        </div>
                                        <div class="col-md-7 job_sec_p">
                                            <p><?php echo $values->conditions; ?></p>
                                        </div>
                                    <button type="button" style="margin-top: 25px;"  class="pull-right btn btn-primary m-top-10" data-toggle="collapse" data-target="#applynow">Apply Now</button>
                                  
                                    
                                <div id="applynow" class="collapse">
                                   <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                        <form action="" method="post" enctype="multipart/form-data">
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="name" class='control-label'>Name</label>
                                                <input type="name" class="form-control" name="name" id="name" placeholder="Enter Your Name" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="email" class='control-label'>E-mail</label>
                                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter Your E-mail" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="number" class='control-label'>Phone Number</label>
                                                <input type="number" class="form-control" name="number" id="number" placeholder="Enter Your Phone Number" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="position" class='control-label'>Position Applied</label>
                                                <input type="name" class="form-control" name="position" id="position" placeholder="Enter Position Applied" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="jobtype" class='control-label'>Job Type</label>
                                                <select class="form-control" name="jobtype" id="jobtype" required>
                                                  <option>Select Your Option</option>
                                                  <option>Full Time</option>
                                                  <option>Part Time</option>
                                                  <option>Internship</option>
                                                  <option>Contract</option>
                                                </select>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="experience" class='control-label'>Experience</label>
                                                <select class="form-control" name="experience" id="experience" required>
                                                  <option>Select Your Option</option>
                                                  <option>Fresher</option>
                                                  <option>Internship</option>
                                                  <option>6 Months</option>
                                                  <option>1 Year</option>
                                                  <option>2 Years</option>
                                                  <option>3 Years</option>
                                                  <option>4 Years</option>
                                                  <option>5 Years</option>
                                                  <option>6 Years</option>
                                                  <option>> 6Years</option>
                                                </select>
                                              </div>
                                          </div>
                                      </div>
                                       <div class="col-md-12">
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="cctc" class='control-label'>Current CTC Per Annum</label>
                                                <input type="number" class="form-control" name="cctc" id="cctc" placeholder="Enter Your current ctc" required>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group required">
                                                <label for="expected" class='control-label'>Expected CTC Per Annum</label>
                                                <input type="number" class="form-control" name="expected" id="expected" placeholder="Enter Your expected ctc" required>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group required">
                                                <label for="previous" class='control-label'>Previous Company Name</label>
                                                <input type="text" class="form-control" name="previous" id="previous" placeholder="company Name" required>
                                              </div>
                                        </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="image">Upload Resume</label>
                                                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="fileHelp" required>
                                                <!--<small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>-->
                                              </div> 
                                          </div>
                                      </div>
                                      <div class="col-md-12">
                                          <div class="col-md-12">
                                              <div class="form-group required">
                                                <label for="jobdescription" class='control-label'>Technical Skills</label>
                                                <textarea class="form-control" name="jobdescription" id="jobdescription" rows="7" required></textarea>
                                              </div>
                                          </div>
                                            <div class="transition colelem ss">
                                                <button class="submit btn btn_sub1">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    
                                  </div>
                                    </div>

                                </div>
                                
                            </div>
                            <?php } ?>
                            <!-- single blog right start -->
                            <div class="col-sm-4 wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="single-blog-right">
                                    <span class="single-blog-right-serach"><input type="text" placeholder="Search..."><span class="serch-icon"><a href="#"><i class="fa fa-search"></i></a></span></span>
                                    <div class="single-blog-news-letter">
                                        <h3>News letter</h3>
                                        Enter your email below if you want to
                                        receive our newsletter
                                        <form action="formspree.io/sample@gmail.com">
                                            <input type="text" placeholder="Email address">
                                            <button type="button">Subscribe</button>
                                        </form>
                                    </div>
                                    <div class="single-recent-post">
                                        <h3>Recent Jobs</h3>
                                        <?php foreach($career_recent as $recent){ ?> 
                                        <a href="<?php echo base_url("careers/job/").$recent->id; ?>" class="single-recent-items">
                                            
                                            <img src="<?php echo base_url().$recent->image; ?>" alt="">
                                            <div class="single-post-content">
                                                <h5><?php echo $recent->title; ?></h5>
                                                <span class="alarm-meta"><i class="fa fa-clock-o"></i> <?php echo $recent->reg_date; ?></span>
                                            </div>
                                        </a>
                                        <?php } ?>
                                        
                                        
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="test" class="test bg-grey roomy-60 fix">
                <div class="container">
                    <div class="row">                        
                        <div class="main_test fix">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="head_title text-center fix">
                                    <h2 class="text-uppercase">What Client Say</h2>
                                    <h5>Clean and Modern design is our best specialist</h5>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item fix">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img1.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>Sarah Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft has been working on our projects from the past 2 years. Their work is commendable and we're satisfied with their support.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="test_item1 fix sm-m-top-30">
                                    <div class="item_img">
                                        <img class="img-circle" src="<?php echo base_url()?>assets/images/test-img2.jpg" alt="" />
                                        <i class="fa fa-quote-left"></i>
                                    </div>

                                    <div class="item_text">
                                        <h5>John Smith</h5>
                                        <h6>envato.com</h6>

                                        <p>CoreSoft team provides one of the best services, a top class service in affordable price. Our relationship is cordial and fruitful.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>