<footer id="contact" class="footer action-lage bg-black p-top-80">
                <!--<div class="action-lage"></div>-->
                <div class="container">
                    <div class="row">
                        <div class="widget_area">
                            <div class="col-md-3">
                                <div class="widget_item widget_about">
                                    <h5 class="text-white">About Us</h5>
                                    <img src="<?php echo base_url()?>assets/images/logo.png" class="img-responsive ftr-img">
                                    <p class="m-top-10 ftr">
                                    Core Software Technologies Inc is an emerging, U.S-based consulting company that can provide your business with clear solutions.It specializes in providing state-of-the-art business technology solutions to organizations.</p>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                            <div class="col-md-3">
                                <div class="widget_item widget_service sm-m-top-50">
                                    <h5 class="text-white">Quick Links</h5>
                                    <ul class="m-top-20">
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>">Home</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>About">About</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Careers">Careers</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Employee">Employee Corner</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Blog">Blog</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Contact">Contact</a></li>
                                    </ul>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                            <div class="col-md-3">
                                <div class="widget_item widget_service sm-m-top-50">
                                    <h5 class="text-white">Services</h5>
                                    <ul class="m-top-20">
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Cloud">Cloud Computing</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Iot">IoT</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Aws">AWS</a></li>
                                        <li class="m-top-20"><i class="fa fa-angle-right"></i> <a class="f_a" href="<?php echo base_url()?>Chatbot">Chatbot</a></li>
                                    </ul>
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                            <div class="col-md-3">
                                <div class="widget_item widget_newsletter sm-m-top-50">
                                    <h5 class="text-white">Contact</h5>
                                    <div class="widget_ab_item m-top-20">
                                        <div class="item_icon"><i class="fa fa-home"></i></div>
                                        <div class="widget_ab_item_text">
                                            <h6 class="text-white">Location</h6>
                                            <p class="ftr">666 Plainsboro Road, Suite # 1216, Plainsboro, NJ 08536</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-20">
                                        <div class="item_icon"><i class="fa fa-map-marker"></i></div>
                                        <div class="widget_ab_item_text">
                                            <h6 class="text-white">Branch</h6>
                                            <p class="ftr">100 S. Murphy Ave. Suite 200 Sunnyvale, CA 94086</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-20">
                                        <div class="item_icon"><i class="fa fa-phone"></i></div>
                                        <div class="widget_ab_item_text phone">
                                            <h6 class="text-white">Phone:</h6>
                                            <p class="ftr">1-609-256-8005</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-20">
                                        <div class="item_icon"><i class="fa fa-fax"></i></div>
                                        <div class="widget_ab_item_text phone">
                                            <h6 class="text-white">Fax:</h6>
                                            <p class="ftr">1-609-256-8004</p>
                                        </div>
                                    </div>
                                    <div class="widget_ab_item m-top-20">
                                        <div class="item_icon"><i class="fa fa-envelope-o"></i></div>
                                        <div class="widget_ab_item_text mail">
                                            <h6 class="text-white">Email:</h6>
                                            <p class="ftr">info@coresofttech.com</p>
                                        </div>
                                    </div>
                                    <!--<ul class="list-inline m-top-20">
                                        <li>-  <a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a>  - </li>
                                    </ul>-->
                                </div><!-- End off widget item -->
                            </div><!-- End off col-md-3 -->
                        </div>
                    </div>
                </div>
                <div class="main_footer fix bg-mega text-center p-top-40 p-bottom-30 m-top-80">
                    <div class="col-md-12">
                        <p class="wow fadeInRight ftr" data-wow-duration="1s">
                            Copyrights @ 2018, All Rights Reserved.
                            Developed by <a target="_blank" href="http://www.mbrinformatics.com/">MBR Informatics PVT LTD..</a> 
                        </p>
                    </div>
                </div>
            </footer>
        </div>
        <script src="<?php echo base_url()?>assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/owl.carousel.min.html"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url()?>assets/css/slick/slick.js"></script>
        <script src="<?php echo base_url()?>assets/css/slick/slick.min.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.collapse.js"></script>
        <script src="<?php echo base_url()?>assets/js/bootsnav.js"></script>
        <script src="<?php echo base_url()?>assets/js/plugins.js"></script>
        <script src="<?php echo base_url()?>assets/js/main.js"></script>
    </body>
</html>
