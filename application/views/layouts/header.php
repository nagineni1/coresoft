<!doctype html>
<html>
<head>
        <meta charset="utf-8">
        <title>Coresoft Technologies</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.html"> 
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.theme.html"> 
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/slick/slick.css"> 
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/slick/slick-theme.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/iconfont.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootsnav.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/responsive.css" />
        <script src="<?php echo base_url()?>assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>assets/images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url()?>assets/images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
    <style>

    </style>
    </head>
    <body data-spy="scroll" data-target=".navbar-collapse">
        <!--<div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_four"></div>
                </div>
            </div>
        </div>-->
        <div class="culmn">
            <nav class="navbar navbar-default bootsnav navbar-fixed">
                <!--<div class="navbar-top bg-grey fix">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="navbar-callus text-left sm-text-center">
                                    <ul class="list-inline">
                                        <li><a href="#"><i class="fa fa-phone"></i> Call us: 1234 5678 90</a></li>
                                        <li><a href="#"><i class="fa fa-envelope-o"></i> Contact us: your@email.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="navbar-socail text-right sm-text-center">
                                    <ul class="list-inline">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="top-search">
                    <div class="container">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <!--<div class="container"> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="index.html">
                            <img src="<?php echo base_url()?>assets/images//logo.png" class="logo" alt="">
                        </a>

                    </div>
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">
                             <li class="nav-item active">
                        <a class="nav-link" href="#">Home</a>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">The Company</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="#">Who We are</a>
                          <a class="dropdown-item" href="#">Vision</a>
                          <a class="dropdown-item" href="#">Mission</a>
                          <a class="dropdown-item" href="#">Team</a>
                          <a class="dropdown-item" href="#">Careers</a>
                        </div>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Solutions</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">IOT</a>
                          <a class="dropdown-item" href="#">Mobile Framework</a>
                          <a class="dropdown-item" href="#">Worldwide IPTV</a>
                          <a class="dropdown-item" href="#">Networking</a>
                        </div>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="#">Cloud Computing</a>
                          <a class="dropdown-item" href="#">Consulting</a>
                          <a class="dropdown-item" href="#">Training</a>
                          <a class="dropdown-item" href="#">Development</a>
                        </div>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Industries</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="#">Consumer Electronics Services</a>
                          <a class="dropdown-item" href="#">Healthcare</a>
                          <a class="dropdown-item" href="#">Financial</a>
                          <a class="dropdown-item" href="#">Construction</a>
                        </div>
                      </li>
                        <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Employee Corner</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Blogs</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="#">Blog 1</a>
                              <a class="dropdown-item" href="#">Blog 2</a>
                            </div>
                          </li>
                        </ul>
                    </div>
                </div> -->
                <div class="container"> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url()?>">
                            <img src="<?php echo base_url()?>assets/images//logo.png" class="logo" alt="">
                        </a>

                    </div>
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">
                             <li class="nav-item  <?php if($this->router->fetch_class() == "welcome") echo "active"; ?>">
                        <a class="nav-link" href="<?php echo base_url()?>">Home</a>
                      </li>
                        <li class="nav-item <?php if($this->router->fetch_class() == "About") echo "active"; ?>"><a class="nav-link" href="<?php echo base_url()?>About">About</a></li>
                            <li class="nav-item dropdown <?php if($this->router->fetch_class() == "Services" || $this->router->fetch_class() == "iot" || $this->router->fetch_class() == "cloud" || $this->router->fetch_class() == "aws" || $this->router->fetch_class() == "chatbot") echo "active"; ?>"><a class="nav-link" href="#">Services <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li class="nav-item"><a style="text-transform:none;" class="nav-link" href="<?php echo base_url()?>Services/iot">IoT</a></li>
                          <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Services/cloud">Cloud</a></li>
                          <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Services/aws">Aws</a></li>
                          <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Services/chatbot">Chatbot</a></li>
                        </ul>    
                        </li>
                        <li class="nav-item <?php if($this->router->fetch_class() == "Careers") echo "active"; ?>"><a class="nav-link" href="<?php echo base_url()?>Careers">Careers</a></li>
                        <li class="nav-item <?php if($this->router->fetch_class() == "Employee") echo "active"; ?>"><a class="nav-link" href="<?php echo base_url()?>Employee">Employee Corner</a></li>
                        
                        <li class="nav-item  <?php if($this->router->fetch_class() == "Blog") echo "active"; ?>"><a class="nav-link" href="<?php echo base_url()?>Blog">Blog</a>
                            <!--<ul class="dropdown-menu">
                              <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Blog/blog_details">Keeping it green: IoT for sustainable retail</a></li>
                              <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Blog/blog_details1">How to avoid asset meltdown in nuclear power plants</a></li>
                              <li class="nav-item"><a class="nav-link" href="<?php echo base_url()?>Blog/blog_details2">4 inspiring stories from the front lines of asset maintenance</a></li>
                            </ul>  -->  
                        </li>
                        <li class="nav-item <?php if($this->router->fetch_class() == "Contact") echo "active"; ?>"><a class="nav-link" href="<?php echo base_url()?>Contact">Contact</a></li>
                        </ul>
                    </div>
                </div> 
            </nav>