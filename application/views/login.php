<?php include 'layouts/header.php'; ?>
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 mar0">
                            <img src="<?php echo base_url()?>assets/images/career_banner.jpg" class="img-responsive inner_banner">
                        </div>
                    </div>
                </div>
            </section>
            <section id="business" class="business roomy-70">
                <div class="container">
                    <div class="row">
                        
                        <?php if(isset($success)) echo '<div class="alert alert-success">'.$success.'</div>'; ?>
                        <form id="contact-form" method="POST" enctype="multipart/form-data">

                        <div class="messages"></div>

                        <div class="controls">
                            <?php if(isset($error)) echo '<div class="alert alert-danger">'.$error.'</div>'; ?>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" type="text" name="email" class="form-control" placeholder="Please enter email" required="required" data-error="email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="pwd">Password</label>
                                        <input id="pwd" type="password" name="pwd" class="form-control" placeholder="Please enter password" required="required" data-error="password is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="action_btn text-left sm-text-center">
                                     <input type="submit" class="btn btn-default" value="Submit">
                                </div>
                                </div>
                            </div>
                        </div>

                    </form>
                    </div>
                </div>
            </section><!-- End off Business section -->
            <section id="action" class="action bg-primary roomy-40">
                <div class="container">
                    <div class="row">
                        <div class="maine_action">
                            <div class="col-md-8">
                                <div class="action_item text-center">
                                    <h2 class="text-white text-uppercase">LET'S GET STARTED ON YOUR PROJECT</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="action_btn text-left sm-text-center">
                                    <a href="<?php echo base_url()?>Contact" class="btn btn-default">Get in touch</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <?php include 'layouts/footer.php'; ?>