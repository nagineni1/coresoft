-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2018 at 06:38 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `core`
--

-- --------------------------------------------------------

--
-- Table structure for table `applyjob`
--

CREATE TABLE `applyjob` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `number` varchar(30) NOT NULL,
  `position` varchar(50) DEFAULT NULL,
  `jobtype` varchar(2250) DEFAULT NULL,
  `experience` varchar(50) DEFAULT NULL,
  `cctc` varchar(50) DEFAULT NULL,
  `expected` varchar(50) DEFAULT NULL,
  `previous` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `jobdescription` varchar(225) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applyjob`
--

INSERT INTO `applyjob` (`id`, `name`, `email`, `number`, `position`, `jobtype`, `experience`, `cctc`, `expected`, `previous`, `image`, `jobdescription`, `reg_date`) VALUES
(5, 'gfhbfdh', 'ghj@fg.hkj', '6576', 'ghtbhnbg', 'Internship', '1 Year', '6758', '7876878', 'nmhjb l,kh', 'uploads/linkss.docx', 'hjlkil.ok.l;', '2018-01-23 07:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `pwd` varchar(225) NOT NULL,
  `address` varchar(225) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `pwd`, `address`, `reg_date`) VALUES
(1, '', 'admin@admin.com', 'admin', '', '2018-01-23 04:58:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `id` int(6) UNSIGNED NOT NULL,
  `image` varchar(225) NOT NULL,
  `title` varchar(225) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `description` longtext,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`id`, `image`, `title`, `name`, `description`, `reg_date`) VALUES
(2, 'uploads/blog.jpg', 'Keeping it green: IoT for sustainable retail', 'Maashok kahn ', 'Stuck for a New Year’s resolution? Shopping more sustainably might be a good one. Especially if you’re suffering pangs of guilt following a Christmas plastic binge (think Christmas cracker toys and disposable cutlery, for instance.) Sustainable retail means many things, from opting for responsibly farmed produce, to keeping an eye on your carbon footprint. And retailers themselves have a part to play, by managing their stores with waste reduction in mind. With all this, the IoT can help. Let’s take a look at some sustainable shopping practices that might find their way into 2018.  Reducing waste in the retail store Traditional stores and shopping malls face a challenge when it comes to operating sustainably. That’s due partly because a large physical footprint, temperature controlled interiors and lighting are somewhat unavoidable. Such a composition makes for high energy consumption because of sheer size and long opening hours.  As there are so many variables around how a store is used. From the number of people who are inside, to the weather outside, it can be difficult to apportion energy appropriately. It’s not easy to plan which parts of the store will be most visited, and which unused, at what times.  Then, there’s the equipment to maintain. Factor in a fridge with a door that refuses to close, and suddenly there’s yet more wasted energy. The fridge has to work harder to keep its contents cool. And the heating system has to compensate for the sudden drop in temperature.  But this needn’t be the case. By making stores ‘connected’, retailers can more efficiently manage energy and utilities to meet actual demand and avoid unnecessary waste.  Helping shoppers make wiser choices Of course, sustainable retail practices go beyond the stores themselves. As consumers, we also have a choice about the products that we buy. We can choose produce resulting from sustainable agriculture practices, that promote animal welfare and limit use of pesticides and antibiotics, for example. We can opt to buy Fairtrade, or prioritize goods with a low carbon footprint.  It seems that people are generally willing to choose sustainable goods over unethically manufactured ones, even if they come with a heftier price tag. According to a survey by Nielsen, around 75 percent of Millennials and Gen Zers would be willing to pay more for goods that are sustainably sourced.  The spirit is willing, then. But the tricky part is getting the information to help us make these choices. Sure, some labels include small print that tells you where the stuff has come from. But who’s got time to read all that detail? Here, the IoT could help make the process more streamlined, especially when we enlist the help of a smartphone app.  Shoppers could adopt a point-and-shoot method – scanning smart barcodes to instantly pull up key information. You could even make it super simple, by combining various attributes into a 1-5 rating system. A score of 1 is bad, while 5 is good. The rating could take into account factors to form the final score. These could include mileage, carbon footprint, point of origin, and whether or not food produce comes from a farm with a good reputation for humane livestock husbandry.  Hundreds of data points could be combined to give a robust rating that fairly evaluates sustainability based on multiple factors. That way, shoppers can get the key information at a glance. No ploughing through the details. And they’re safe knowing the scores are the result of thorough research.', '2018-01-25 05:02:54'),
(4, 'uploads/blog-img-2.jpg', 'How to avoid asset meltdown in nuclear power plants', ' Jhon Hegdh ', 'We can all imagine the complexity surrounding the workings of a nuclear power plant. Given the level of safety, reliability and regulatory requirements they face, every operation and process needs to be designed and managed with utmost care. The same is the case with asset management.  Asset management plays a critical role in the operations of a nuclear power plant for the following reasons: It establish processes to help improve reliability by ensuring the lowest downtime of assets possible  It helps manage the complexity of assets to improve visibility and maintenance optimization. This ensures longer asset life and high returns on investment. This is especially critical in a nuclear power plant where equipment and infrastructure can come at a high cost.  It helps protect the power supply, ensuring it is always available to meet demand  Data from the IoT enables predictive maintenance Modern nuclear power plant designs include more IoT sensors than their older counterparts. With these sensors, operators have access to raw data , which they can feed into a predictive monitoring solution and view actionable insights in real time.  Data collected and analyzed can provide a precise picture of an asset’s state of health (Good, Fair, or Poor) – enabling the discovery of failures and potential failures that otherwise would have been impossible to spot. Predictive maintenance, based on this analysis, enables nuclear power plants to be more proactive and confident in their asset maintenance – potentially avoiding disasters or outages.  With this added insight, plant operators have an advantage in terms of the continuous operation of plant assets and better scheduling of maintenance tasks – which translates into reduced costs.  A specialized solution for the Energy & Utilities industry Asset and operational management for nuclear power is unique and requires specialized enterprise asset management software. CoresoftIoT for Energy and Utilities is an open analytics solution that includes a wide range of capabilities to meet current and future needs of nuclear power providers. CoresoftIoT for Energy and Utilities is built on a foundation of data integration. It can capture and aggregate all relevant sources of information required to run the most advanced analytics across a wide variety of use cases.  – Out-of-the-box utility industry applications. They apply a wide range of analytical capabilities to assess asset health and risk—historically and in real-time. It can verify connectivity models in a cost effective way. It can also provide situational awareness—from the equipment level to the grid level – and employ predictive maintenance to proactively address impending asset degradation or failure.', '2018-01-25 05:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_careers`
--

CREATE TABLE `tbl_careers` (
  `id` int(6) UNSIGNED NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `title` varchar(225) NOT NULL,
  `location` varchar(225) DEFAULT NULL,
  `responsibilities` longtext,
  `experience` longtext,
  `conditions` longtext,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_careers`
--

INSERT INTO `tbl_careers` (`id`, `image`, `title`, `location`, `responsibilities`, `experience`, `conditions`, `reg_date`) VALUES
(1, 'uploads/w.png', 'Web Designer', 'New Jearsy', 'Produce web site components that meet editorial and production requirements using a variety of coding tools, graphic software packages and content management systems.\r\n\r\nProvide technical solutions for promotional and informational Web sites, print publications.\r\n\r\nDevelops, modifies, and implements Web sites. Coordinates with content developers to ensure consistency of layout.\r\n\r\nConducts ongoing quality control on Web sites using code validation and hyperlink checking software.', 'Web design and document design/production experience including preparation of presentations, marketing communications, product and brand development and type.\r\n\r\n', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.', '2018-01-22 12:07:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applyjob`
--
ALTER TABLE `applyjob`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_careers`
--
ALTER TABLE `tbl_careers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applyjob`
--
ALTER TABLE `applyjob`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_careers`
--
ALTER TABLE `tbl_careers`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
